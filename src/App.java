import com.devcamp.j50_20_basicjava.NewDemoJava;

public class App {
    public static void main(String[] args) throws Exception {
        /*
         * Cmt nhiều dòng (theo khối)
         * ví dụ sử dụng biến string
         */
        System.out.println("Hello, World!");
        String strName = new String("Devcamp");
        System.out.println(strName); // cmt 1 dòng: in biến strName ra console
        /*
         * Ví dụ sử dụng các phương thức của lớp String
         */
        System.out.println("Chuyển về chữ thường: ToLowerCase: " + strName.toLowerCase());
        System.out.println("Chuyển về chữ hoa: ToUpperCase: " + strName.toUpperCase());
        System.out.println("Chiều dài của chuỗi: length: " + strName.length());
    
        /*
         * Gọi phương thức của class khác
         */
        // Phương thức tĩnh => gọi trực tiếp Class.TenPhuongThuc
        NewDemoJava.name(23, "Ho Boi Boi");
        
        // Phuong thức  thường => Tạo đối tượng rồi mới gọi phương thức
        NewDemoJava demo = new NewDemoJava();
        demo.name("Tran Van Manh");
        System.out.println(demo.name("Nguyen Anh Minh", 35));
    }
}
