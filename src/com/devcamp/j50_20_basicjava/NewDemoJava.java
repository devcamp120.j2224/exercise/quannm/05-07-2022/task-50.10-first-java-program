package com.devcamp.j50_20_basicjava;

public class NewDemoJava {
    public static void main(String[] args) {
        System.out.println("New Demo Java Class");
        
        NewDemoJava.name(30, "Nguyen Minh Quan"); // Gọi phương thức tĩnh: Dùng trực tiếp Class.tên phương thức

        /*
         * Gọi phương thức thường: dùng qua đối tượng (khởi tạo => gọi phương thức)
         */
        NewDemoJava demo = new NewDemoJava();
        demo.name("Tran Van Manh");
        System.out.println(demo.name("Nguyen Anh Minh", 35));
    }

    public void name(String strName) {
        System.out.println(strName);
    }

    public String name(String strName, int age) {
        String strResult = "My name is " + strName + ", my age is " + age;
        return strResult;
    }

    public static void name(int age, String strName) {
        String strResult = "My name is " + strName + ", my age is " + age;
        System.out.println(strResult);
    }
}
